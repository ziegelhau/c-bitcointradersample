﻿using System.Collections.Generic;
using GlobalEntities.Entities;
using Context.BitcoinDatabase;
using Queries.InputVariablesEntity.InputVariablesQuery;

namespace Queries.InputVariablesEntity
{
    public class InputVariablesQueries
    {
        BitcoinContext _context;

        public InputVariablesQueries(BitcoinContext context)
        {
            _context = context;
        }

        public IEnumerable<InputVariablesSlopeAlgorithm> GetInputVariableQuery()
        {
            return new GetInputVariablesOrderedBySecondLimitAscendingQuery(_context.InputVariablesSlopeAlgorithm).Execute();
        }
    }
}
