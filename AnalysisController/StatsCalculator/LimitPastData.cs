﻿using System.Collections.Generic;
using GlobalEntities.Entities;

namespace AnalysisController.StatsCalculator
{
    public class LimitDataClass
    {
        /// <summary>
        /// Limits data point list within pastSecondsToAverageOver seconds from dataPoints.xValue[startIndex]
        /// Assumes data is minute seperated
        /// </summary>
        /// <param name="dataPoint"> all of the x (unix time stamp) and y (weighted currency) points </param>
        /// <param name="startPoint"> Contains point to compare to </param>
        /// <param name="getDataWithinThesePastSeconds"> Number of seconds to retrieve points within </param>
        /// <param name="limitDataPercent"> Minimum number of data needed (with 1 being max amount of points) </param>
        /// <returns> A range from the given point, and a boolean indicating if there is enough points to 
        /// accurately do calculations with </returns>                        
        // Pass in index instead of dataPoint because otherwise we need to do i = dataPoints.indexOf(dataPoint) which is very slow        
        //public List<LongDataPoint> LimitData(List<LongDataPoint> dataPoints, int startIndex, int pastSecondsToAverageOver)
        public (List<LongDataPoint>, bool) LimitData(List<LongDataPoint> dataPoints, int startIndex, int getDataWithinThesePastSeconds, double limitDataPercent)
        {
            var limitedDataPoints = new List<LongDataPoint>();            
            var i = startIndex;
            while(i >= 0 && dataPoints[i].XValue >= dataPoints[startIndex].XValue - getDataWithinThesePastSeconds)
            {
                limitedDataPoints.Add(dataPoints[i]);
                i--;
            }
            limitedDataPoints.Reverse();
            //return limitedDataPoints;
            return (limitedDataPoints, HaveEnoughPoints(limitedDataPoints, getDataWithinThesePastSeconds, limitDataPercent));
        }
        /// <summary>
        /// (Assumes data is minute seperated) Given limited data set and time frame, check if we have enough points to do calculations
        /// </summary>
        /// <param name="limitedDataPoints"> List of limited data points
        /// <param name="getDataWithinThesePastSeconds"> Number of seconds to retrieve points within </param>
        /// <param name="limitDataPercent"> Minimum number of data needed (with 1 being max amount of points) </param>        
        /// <returns> Boolean indicating if we have enough data points to continue the calculations </returns>
        public bool HaveEnoughPoints(List<LongDataPoint> limitedDataPoints, int getDataWithinThesePastSeconds, double limitDataPercent)
        {                        
            var numOfPoints = limitedDataPoints.Count;
            // 1 data point is not enough to do calculations
            if (numOfPoints == 1)
            {
                return false;
            }
            // Estimate that we need 80% of the points to do a calculation
            return (numOfPoints > ((getDataWithinThesePastSeconds / 60 + 1) * limitDataPercent)) ? true : false;
        }
    }
}
